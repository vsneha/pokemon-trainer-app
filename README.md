# PokemonTrainerApp
This project was generated with Angular CLI version 12.2.3.

# Development server
Run ng serve for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

# Code scaffolding
Run ng generate component component-name to generate a new component. You can also use ng generate directive|pipe|service|class|guard|interface|enum|module.

# Build
Run ng build to build the project. The build artifacts will be stored in the dist/ directory.

# Further help
To get more help on the Angular CLI use ng help or go check out the Angular CLI Overview and Command Reference page.

# Description
This project is made as an assigment for Noroff Fullstack bootcamp. There are some redundant components, however they can be usefull for further represantation of the POKI-API, because there is so much potential. Its an app where you can login with the username, which will redirected to the catalouge page where you can find the  list of pokomons. when the trainer selects the desired pokomon it appears in the trainer page. You can also delete the pokemon if you dont want it in your profile. its a SPA with router pages. Only local storage is used for information. all will be lost, when you reaload the APP.

