import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-catalogue-details',
  templateUrl: './catalogue-details.component.html',
  styleUrls: ['./catalogue-details.component.scss'],
})
export class CatalogueDetailsComponent implements OnInit {
  api$: any;
  abilities:any[] = [];
  stats: any[] = [];
  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonService
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      console.log(params);
      const { url } = params;
      this.pokemonService.getPokemonDetails(url).subscribe((data: any) => {
        const { abilities = [], stats = [] } = data;
        abilities.forEach((a: any) => {
          const { ability: name  } = a;
          this.abilities.push(name);
        });

        console.log(abilities,this.abilities);


        stats.forEach((s: any) => {
          const { stat: name } = s;
          this.stats.push(name);
        });
      });
    });
  }
}
