import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PokemonService } from '../services/pokemon.service';
import { Pokemon } from "../models/pokemon.model";
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss'],
})
export class CatalogueComponent implements OnInit {
  pokemons: any;
  trainerListAsHashById = {};

  constructor(private pokemonService: PokemonService) {}

  ngOnInit(): void {
    
    const url = environment.pokemanApiUrl+"pokemon?limit=100&offset=0";
    this.getPokemonList(url);
  }
  getPokemonList(url: any) {
    this.pokemonService.getPokemonList(url).subscribe((data) => {
      this.pokemons = data;
      this.getTrainerList();
    });
  }
   // dispalying all the pokemons to select...
  addToTrainer(pokemonObject: any, index: any) {

    const loggedInProfile = JSON.parse(localStorage.getItem("loggedInProfile")|| '{}');
    const { pokemon = [] } = loggedInProfile;

    let p = {} as Pokemon;
    p.id = +index+1;
    p.name=pokemonObject.name;
    p.avatar="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + index+1 +".png";
    p.url=pokemonObject.url;
    pokemon.push(p);
    loggedInProfile.pokemon = pokemon;

    console.log(loggedInProfile);

    // its adding to pokemon array and localstorage and database
    this.pokemonService.updateTrainer(loggedInProfile).subscribe(
      data => {
        localStorage.setItem("loggedInProfile", JSON.stringify(loggedInProfile));
        this.getTrainerList();
      }
    )

    // let trainerListAsParse: any[] = JSON.parse(
    //   localStorage.getItem('trainerList') || '[]'
    // );
    // const catelogueById = trainerListAsParse.find(
    //   (e: any) => e.id === pokemon.id
    // );
    // if (catelogueById) {
    //   trainerListAsParse = trainerListAsParse.filter(
    //     (e: any) => e.id !== pokemon.id
    //   );
    // } else {
    //   pokemon['isCollected'] = true;
    //   trainerListAsParse.push(pokemon);
    // }
    // localStorage.setItem('trainerList', JSON.stringify(trainerListAsParse));
    // this.trainerListAsHashById = this.arrayToHash(trainerListAsParse);
  }

 // here we aretrying to store the id, name , url and the image value inside the array 
 // of the pokenon database.
  arrayToHash = (array: any[], id: string = 'id') =>
    array.reduce((obj, item) => ((obj[item[id]] = item), obj), {});

  getTrainerList = () => {
    let trainerListAsParse: any = JSON.parse(
      localStorage.getItem('loggedInProfile') || '{}'
    );
    const { pokemon = [] } = trainerListAsParse;
    this.trainerListAsHashById = this.arrayToHash(pokemon);
    console.log(this.trainerListAsHashById);
    
  };
}
