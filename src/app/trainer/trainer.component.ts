import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {

  pokemons: any[];
  isLoading: boolean;


  constructor(
    private authService: AuthService,
    private pokemonService: PokemonService
  ) { }

  ngOnInit(): void {

    this.getTrainerList()
  }

  getTrainerList() {
    this.isLoading = true;
    const trainer = JSON.parse(
      localStorage.getItem('loggedInProfile') || '{}'
    );
    const { username = ""} = trainer;
    this.authService.getTrainersByUserName(username).subscribe(
      (data: any) => {
        const { pokemon = [] } = data[0];
        this.pokemons = pokemon;
        console.log(this.pokemons);
        this.isLoading = false;

        


      }
    )

  }

  remove = (pokemon: any, index: any) =>{

    const { id } = pokemon;

    console.log(id, this.pokemons[0].id);

    const fil = this.pokemons.filter(
      e => e.id == id);
    console.log(fil);
    
    
    let loggedInProfile = JSON.parse(localStorage.getItem("loggedInProfile")|| '{}');

    
    loggedInProfile.pokemon = this.pokemons.filter(e => Number(e.id) !== +id);


    this.pokemonService.updateTrainer(loggedInProfile).subscribe(
      data => {
        localStorage.setItem("loggedInProfile", JSON.stringify(loggedInProfile));
        this.getTrainerList()
      }
    )



  }

}
