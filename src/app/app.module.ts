import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { TrainerComponent } from './trainer/trainer.component';
import { CatalogueComponent } from './catalogue/catalogue.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { OutgoingInterceptor } from './httpInterceptor/outgoingInterceptor';
import { CatalogueDetailsComponent } from './catalogue/catalogue-details/catalogue-details.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    TrainerComponent,
    CatalogueComponent,
    PageNotFoundComponent,
    NavbarComponent,
    CatalogueDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: OutgoingInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
