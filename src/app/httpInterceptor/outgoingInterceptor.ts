import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpEvent,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { environment } from '../../environments/environment';

// when an API call is made , the intercepter will first call the headers and attcach an rest of the API
@Injectable()
export class OutgoingInterceptor implements HttpInterceptor {
  intercept(
    httpRequest: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {  
    return next.handle(httpRequest.clone({ setHeaders: {'X-API-Key': environment.apiKey,'Content-Type': 'application/json'}}));
  }
}
