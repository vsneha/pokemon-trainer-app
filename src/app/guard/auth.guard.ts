import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private routes: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const stateUrl = state.url;
    console.log('state url: ', stateUrl);
    const loggedInDetails: any =
      localStorage.getItem('loggedInProfile') || null;
    if (loggedInDetails) {
      return true;
    }

    return false;
  }
}
