import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() selectedNav: any;

  loggedInProfile = JSON.parse(localStorage.getItem("loggedInProfile") || "{}")

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  logout = () => {
    this.authService.logout().subscribe(
      data => {
        this.router.navigate(['/']);
      }
    )
  }

}
