import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  isSubmitted: boolean;
  form: FormGroup = new FormGroup({
    username: new FormControl(undefined, [Validators.required]),
    pokemon: new FormControl([])
  })

  constructor(
    private authService : AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login = () => {
    this.isSubmitted = true;
    const { status } = this.form;
    if (status === 'VALID') {
      this.authService.getTrainersByUserName(this.form.get('username')?.value).subscribe
      ((data: any) => {
          if (data.length > 0) {
            this.iniatiateLogin(data[0])
          }else {
            this.createNewTrainer(this.form.value)
          }
        }, error =>{

        }
      )

    }
    console.log(this.form.value);
  }

  createNewTrainer = (requestBody: any) => {
    this.authService.createNewTrainer(requestBody).subscribe(
      data => {
        this.iniatiateLogin(data);
      }, error => {

      }
    )
  }

  iniatiateLogin = (requestBody: any) => {
    this.authService.signIn(requestBody).subscribe(
      data => {
        this.router.navigate(['catalogue']);
      }
    )
  }

  get formControl() {
    console.log(this.form)
    return this.form.controls;
  }

}
