import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { of } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  logout() {
    localStorage.clear();
    sessionStorage.clear();
    return of({})
  }
  constructor(private httpClient: HttpClient) {}

  signIn = (loggedInProfile: any) => {
    localStorage.setItem('loggedInProfile', JSON.stringify(loggedInProfile));
    return of(loggedInProfile);
  };

  getTrainersByUserName = (userName: any) => {
    const { apiURL } = environment;
    let finalUrl = `${apiURL}/trainers?username=${userName}`;
    return this.httpClient.get(finalUrl);
  };

  createNewTrainer = (requestBody: any) => {
    const { apiURL, apiKey } = environment;
    let finalUrl = `${apiURL}/trainers`;

    return this.httpClient.post(finalUrl, requestBody);
  };
}
