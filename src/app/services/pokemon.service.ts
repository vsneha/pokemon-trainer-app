import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  

  constructor(
    private httpClient: HttpClient
  ) {}

  getPokemonList = (url: any) => {
    return this.httpClient.get(url);
  }

  getPokemonDetails = (url: any) => {
    return this.httpClient.get(url);
  }

  updateTrainer({id="", pokemon = []}) {
    const { apiURL, apiKey } = environment;
    let finalUrl = `${apiURL}/trainers/${id}`;
    let body = {
      pokemon: pokemon,
    }
    return this.httpClient.patch(finalUrl, body)
  }
}
